# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [2.5.5] - 18-05-2021
- Enables Training of PositiveUnlabelled.java with no unlabelled datum

## [Released]
## [2.5.4] - 04-03-2021
- Implementation of PositiveUnlabelled.java
